import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Section from '../views/Section.vue'

var Emitter = require('tiny-emitter');
var emitter = new Emitter();

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/:guide/:section',
    name: 'section',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Section
  }
]

const router = new VueRouter({
    routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        let position = { x: 0, y: 0 };

        if (savedPosition) {
            position = savedPosition
        }

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(position)
            }, 300)
        })
    }
})

export default router
